# coding=utf-8
# Usage : python botperceval.py [nom du répertoire qui contient le fichier avec les infos de connexion]

from mastodon import Mastodon
from time import sleep
from random import choice
import csv
import sys
sys.path.insert(0, '..')
from botinit import get_api

current_directory = sys.argv[1]

api, MASTODON_USER_ID = get_api(current_directory + "keys.txt")

# Get things done
proverbes = []
with open('proverbes.txt') as csv_file:
    reader = csv.reader(csv_file)
    for row in reader:
        proverbes.append(row)

while True:
    debut = choice(proverbes)[0]
    fin = choice(proverbes)[1]
    toot = debut + fin

    api.status_post(
        toot,
        visibility='unlisted'
    )

    sleep(86400)
