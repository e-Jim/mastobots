import random

def random_line(filename):
    line_num = 0
    selected_line = ''
    with open(filename) as f:
        while 1:
            line = f.readline()
            if not line: break
            line_num += 1
            if random.uniform(0, line_num) < 1:
                selected_line = line
    return selected_line

def poem(f1, f2, f3):
    res = random_line(f1) + \
          random_line(f2) + \
          random_line(f3)
    return res


def main():
    print(poem())

if __name__ == "__main__":
    main()
