# coding=utf-8

"""
Script permettant de lancer le bot.

Démarrer le bot en français avec la commande

```
python bot.py ./ fr
```

Il doit y avoir un sous-dossier nommé `fr` contenant le fichier `keys.txt` avec les infos pour se connecter à Mastodon.

"""
from time import sleep
import datetime
import sys

from gen import check, get_question, get_replies, parse_post, post_answer, post_question, read_file

sys.path.insert(0, '..')
from botinit import get_api

# Connect to Mastodon
current_directory = sys.argv[1]

language = sys.argv[2]

api, MASTODON_USER_ID = get_api(current_directory + language + "/keys.txt")

# Read questions from the database
questions, toots = read_file(language)
indexes = list(range(len(questions)))

# Post first toot
indexes, (current_question, current_answers, current_explanation) = get_question(questions, indexes)
post = post_question(api, current_question, language)
tempo = 300

since_post = api.notifications(limit=1)[0] # When checking for new replies, the bot will look for replies more recent than `since_post`


while True:
    # get answers and compare to solution
    replies = get_replies(api, since_post)
    try:
        since_post = replies[0]
    except IndexError:  # This means there are no new replies
        pass

    # check if someone got the right answer
    ok = False
    for rep in reversed(replies): # Read replies in chronological order
        print(parse_post(rep.status.content))
        if not ok: # Stop going through replies when a correct answer is found
            ok_toot = rep
            ok = check(ok_toot.status.content, current_answers, language)

    # when someone gets it right
    if ok:
        print('ok', parse_post(ok_toot.status.content))
        # fav all correct toots
        for rep in replies:
            if check(rep.status.content, current_answers, language):
                api.status_favourite(rep.status)

        # give the answer
        post_answer(
            api, post,
            current_answers, current_explanation,
            language, toots, found=True, ok_toot=ok_toot)
        since_post = replies[0]

        # ask a new question
        indexes, (current_question, current_answers, current_explanation) = get_question(questions, indexes)
        post = post_question(api, current_question, language)
        tempo = 300

    else:
        # if nobody found the answer after a long time (8 hours)
        if post.created_at.replace(tzinfo=None) + datetime.timedelta(hours=8) < datetime.datetime.now():
            post_answer(
                api, post,
                current_answers, current_explanation,
                language, toots, found=False)

            indexes, (current_question, current_answers, current_explanation) = get_question(questions, indexes)
            sleep(100)
            post = post_question(api, current_question, language)
            tempo = 300
            try:  # in case people answered the previous question but got it wrong
                since_post = replies[0]
            except IndexError:
                pass

        else:
            pass

    sleep(tempo)
    # Check 5 for replies minutes after the question was first asked.
    # Then only check for replies every 15 minutes.
    if tempo == 300:
        tempo = 900
