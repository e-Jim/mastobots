# coding=utf-8
from mastodon import Mastodon
from time import sleep
import datetime
import sys

sys.path.insert(0, '..')
from botinit import get_api

from gen import *


current_directory = sys.argv[1]

api, MASTODON_USER_ID = get_api(current_directory + "/fr/keys.txt")

poll_index = 12
poll_time = 86400

# Read data
with open('polls.json', 'r') as f:
    contents = json.load(f)
all_polls = contents['polls']

while True:
    poll = all_polls[poll_index]
    choices = api.make_poll(poll['choices'], poll_time)

    post = api.status_post(
        poll['question'],
        poll = choices,
        visibility = 'public'
    )
    
    sleep(poll_time)

    if poll['has_image']:
        media = api.media_post('./images/' + poll['image'])
    else:
        media = None
        
    api.status_post(
        poll['answer'],
        visibility = 'public',
        media_ids = media,
        in_reply_to_id = post
    )
    
    poll_index += 1
    print(poll_index)

