# coding=utf-8
"""
Author: Moutmout

CC-BY-SA
"""

from mastodon import Mastodon
from time import sleep
import datetime
import sys

sys.path.insert(0, '..')
from botinit import get_api

from gen import *


current_directory = sys.argv[1]
try:
    idx = int(sys.argv[2])
except:
    idx = 0

api, MASTODON_USER_ID = get_api(current_directory + "keys.txt")

# Get things done
questions = read_file('comptes.csv')

# first toot
since_post = api.notifications()[0]
current_input, current_output = get_question(questions, idx)
print(current_input, current_output)
post = post_question(api, current_input, current_output)

while True:
    print('check replies')
    # get answers and compare to solution
    replies = [e for e in api.notifications(since_id=since_post)
               if e.type == 'mention'
               and e.status.in_reply_to_id == post.id]

    # check if someone got the right answer
    for rep in reversed(replies):
        print(rep.status.content)
        ok = check(rep, current_input, current_output)
        if ok[0]:
            toot_text = 'Réponse valide. Différence = ' + str(ok[1])
        else:
            toot_text = "Réponse invalide:  " + ok[1]

        print(toot_text)
        api.status_post(
            toot_text,
            visibility = 'public',
            in_reply_to_id = rep.status.id)

        if ok[1] == current_output:
            idx += 1
            current_input, current_output = get_question(questions, idx)
            print(current_input, current_output)
            post = post_question(api, current_input, current_output)

        since_post = rep
        
    sleep(60)
