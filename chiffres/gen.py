"""
Author: Moutmout

CC-BY-SA
"""

import random
import json
import re


def read_file(filename):
    contents = open(filename).read().splitlines()
    return contents


def get_question(questions, idx):
    line = questions[idx].split(',')

    return line[1:], line[0]


def post_question(api, input_numbers, output_number):
    toot_text = ', '.join(input_numbers) + '\n\n ⇒  ' + output_number
    post = api.status_post(
        toot_text,
        visibility='public'
    )
    return post


def get_toot_content(toot):
    tokens = toot.replace('>', '<').split('<')
    tokens = [t for t in tokens
              if (t not in ['p', '/p', '/a', '/span', 'span', 'br',
                            '@', '', ' ']
                  and 'a href=' not in t
                  and 'span class=' not in t
                  and 'class="' not in t
                  and 'target="' not in t)]
    
    toot_without_balises = '\n'.join(tokens)
    standardized_content = toot_without_balises.replace('×', '*').replace('x', '*').replace('÷', '/')
    print(standardized_content)
    lines = standardized_content.split('\n')
    return lines


def get_numbers(left_term):
    a = left_term.replace('+', ',').replace('-', ',').replace('*', ',').replace('/', ',')
    return [int(e) for e in a.split(',')]


def check(toot, input_numbers, output_number):
    # Important ! Répondre avec une opération par ligne !
    input_numbers = list(input_numbers)
    
    toot_content = get_toot_content(toot.status.content)
    print('----')
    print(toot_content)

    ok = False # No mathematical operation has been found yet
    for line in toot_content:
        try:
            a, b = line.split('=')
        except ValueError: # if is not exactly one '=' in the line
            print("no '=' sign in ", line)
            try:
                res = eval(line)
            except:
                print('no operation in: ', line)
            else:
                a = line
                b = str(res)
                ok = True
        else:
            ok = True

        if ok:
            # Check the operation gives the indicated result
            try:
                tmp = eval(a) == eval(b)
            except NameError:
                print("can't eval")
                tmp = True
            if not tmp:
                return False, "erreur d'arithmétique"
            
            # Make sure only authorized numbers are used and remove them from the list
            numbers = get_numbers(a)
            for n in numbers:
                try:
                    input_numbers.remove(str(n))
                except ValueError:
                    return False, 'nombre interdit :  ' + str(n)
            
            input_numbers.append(str(eval(b)))
            
    if ok:
        return True, abs(eval(b) - int(output_number))
    else:
        return False, 'aucune opération mathématique détectée'
    
